package main;

import model.*;

import java.awt.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;

public final class SubnodeCalculator implements Runnable {
    private final ExecutorService pool;
    private final FieldNode node;
    private final CalculationHandler calculationHandler;
    private final int prefHeight;

    public SubnodeCalculator(ExecutorService pool, FieldNode node, CalculationHandler calculationHandler) {
        this.pool = pool;
        this.node = node;
        this.calculationHandler = calculationHandler;
        /*if(node.getRootNode() != null) {
            prefHeight = node.getRootNode().getData().getPlayers()[node.getRootNode().getCurrentPlayer()].getBricks().size() < 10 ? 3 : 2;
        }
        else {
            prefHeight = 2;
        }*/

        prefHeight = 2;

        calculationHandler.nodeStarted();
    }

    private double getTurnValue(ShrinkedTurn turn, double percentBricksLeft) {
        boolean[][] actualBrickCoords = turn.getBrick().getForms()[turn.getBrickFormNumber()];
        Point brickStartCoord = turn.getCoord();
        double x = brickStartCoord.x + actualBrickCoords.length / 2.0d;
        double y= brickStartCoord.y + actualBrickCoords[0].length / 2.0d;

        double playerx, playery;

        if(turn.getPlayer().getColor() == Block.BLUE) {
            playerx = 19; playery = 19;
        } else if (turn.getPlayer().getColor() == Block.YELLOW) {
            playerx = 0; playery = 19;
        }else if (turn.getPlayer().getColor() == Block.RED) {
            playerx = 0; playery = 0;
        } else {
            playerx = 19; playery = 0;
        }

        double xvalue = Math.abs(x - playerx) / 20.0d;
        double yvalue = Math.abs(y - playery) / 20.0d;

        //double xmiddle = Math.abs(10.0d - Math.abs(10.0d-x)) / 10.0d;
        //double ymiddle = Math.abs(10.0d - Math.abs(10.0d-y)) / 10.0d;

        //double middle = (xmiddle + ymiddle) / 2.0d;

        double away = (xvalue + yvalue) / 2.0d;

        //return middle * percentBricksLeft + (0.5d - percentBricksLeft/2.0d) * away;

        double brickValue = turn.getBrick().getCornerBlocks()[0].length / 4.0d + turn.getBrick().getForms().length / 8.0d;

        return (away + 0.7*brickValue) / 2.0d;
    }

    private void addTurns(ShrinkedTurn[] posTurns) {
        for(ShrinkedTurn turn : posTurns) {
            GameState gs = node.getData().cloneOnlyPlayerAndField(turn.getPlayer());
            gs.doTurn(turn);

            FieldNode fn = new FieldNode(gs, node, calculationHandler.getAvailablePlayersDetector().getNextPlayerWithThisCurrentPlayer(node.getCurrentPlayer()), turn);

            if(node.getData().getPlayers()[node.getCurrentPlayer()].equals(calculationHandler.getPlayerToCalculateFor())) {
                fn.setScoreRating(getTurnValue(turn, node.getData().getPlayers()[node.getCurrentPlayer()].getBricks().size()));
            }

            node.getChildren().add(fn);

            // ------------------>>>>>>>>>>>>>>>   Mehr als 2 geht nicht! <<<<<<<<<<<<<<<-----------------------------------
            if (node.getHeight() < prefHeight) {
                SubnodeCalculator snc = new SubnodeCalculator(pool, fn, calculationHandler);
                try {
                    pool.execute(snc);
                } catch (RejectedExecutionException e) {
                }
            }
        }
    }

    @Override
    public void run() {
        if(node.getHeight() == prefHeight) {
            double rating = calculationHandler.getAiValueCalculator().ratePossibleTurn(node, calculationHandler.getPlayerToCalculateFor(), calculationHandler.getAvailablePlayersDetector().getNumPlayersLeft());
            node.getParent().setMaxValue(rating, node.getTurnDone());

            calculationHandler.nodeFinishedCallback();
            return;
        }

        ShrinkedTurn[] posTurns = CalculationHelpers.calculatePossibleTurns(node.getData().getField(), node.getData().getPlayers()[node.getCurrentPlayer()]);

        if(!node.getData().getPlayers()[node.getCurrentPlayer()].equals(calculationHandler.getPlayerToCalculateFor())) {
            if(posTurns.length == 0) {
                double rating = calculationHandler.getAiValueCalculator().ratePossibleTurn(node, calculationHandler.getPlayerToCalculateFor(), calculationHandler.getAvailablePlayersDetector().getNumPlayersLeft());
                node.getParent().setMaxValue(rating, node.getTurnDone());
            }
            else {
                addTurns(posTurns);
            }
        }
        else {
            addTurns(posTurns);
        }

        calculationHandler.nodeFinishedCallback();
    }
}
