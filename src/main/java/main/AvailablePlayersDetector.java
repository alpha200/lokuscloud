package main;

public final class AvailablePlayersDetector {
    private boolean[] isOut;
    private int currentPlayer;

    public AvailablePlayersDetector() {
        currentPlayer = 0;
        isOut = new boolean[4];
    }

    public int getNextPlayerWithThisCurrentPlayer(int playerIndex) {
        for (int i = 0; i < 4; i++) {
            playerIndex = ((playerIndex+1) % 4);

            if(!isOut[playerIndex]) {
                return playerIndex;
            }
        }

        return -1;
    }

    public int getAndSetNextPlayer() {
        for (int i = 0; i < 4; i++) {
            currentPlayer = ((currentPlayer+1) % 4);

            if(!isOut[currentPlayer]) {
                return currentPlayer;
            }
        }

        return -1;
    }

    public void setPlayerOut(int playerIndex) {
        System.out.println(playerIndex + " is Out!");

        isOut[playerIndex] = true;
    }

    public int getNumPlayersLeft() {
        int pleft = 4;

        for(boolean aOut : isOut) {
            if(aOut)
                pleft--;
        }

        return pleft;
    }
}
