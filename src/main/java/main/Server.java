package main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

public final class Server implements Runnable {

    private final Logger logger = LoggerFactory.getLogger(Server.class);

    private final ExecutorService pool;
    private final ServerSocket serverSocket;
    private final CalculationHandler calculationHandler;

    public Server(ExecutorService pool, ServerSocket serverSocket, CalculationHandler calculationHandler) {
        this.pool = pool;
        this.serverSocket = serverSocket;
        this.calculationHandler = calculationHandler;
    }

    public void run() {
        try {
            while (true) {
                Socket client = serverSocket.accept();

                pool.execute(new ClientRequestHandler(client, calculationHandler));
            }
        } catch (IOException e) {
            logger.error("Socket listener crashed");
        }
        finally {
            pool.shutdown();
            try {
                pool.awaitTermination(2L, TimeUnit.SECONDS);
                if ( !serverSocket.isClosed() ) {
                    serverSocket.close();
                }
            } catch ( IOException | InterruptedException e ) { }
        }
    }
}
