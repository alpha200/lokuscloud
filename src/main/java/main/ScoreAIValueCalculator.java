package main;

import model.FieldNode;
import model.GameState;
import model.ShrinkedPlayer;

public final class ScoreAIValueCalculator {

    public double ratePossibleTurn(FieldNode fnAfterTurn, ShrinkedPlayer player, int numPlayersLeft) {
        GameState gameState = fnAfterTurn.getData();

        ShrinkedPlayer currentPlayer = null;

        for(ShrinkedPlayer aplayer : gameState.getPlayers()) {
            if(player.equals(aplayer))
                currentPlayer = aplayer;
        }

        assert currentPlayer != null;

        if(currentPlayer.getScore() > 0) {
            return Double.POSITIVE_INFINITY;
        }

        double aivalue = currentPlayer.getScore() * 3;

        for (ShrinkedPlayer sp : gameState.getPlayers()) {
            if(sp.equals(currentPlayer))
                continue;

            aivalue -= sp.getScore();
        }

        double bricksLeftRatio = currentPlayer.getBricks().size() / 21.0d;

        double posValues = 0;

        int curheight = fnAfterTurn.getHeight();
        int numCalc = 0;

        FieldNode nextParent = fnAfterTurn.getParent();

        while (nextParent != null) {
            curheight--;
            if(curheight % numPlayersLeft == 1) {
                posValues += nextParent.getScoreRating();
                numCalc++;
            }

            nextParent = nextParent.getParent();
        }

        if(numCalc == 0) {
            numCalc++;
        }

        double positionValue = posValues / numCalc;

        //System.out.println("ncalc:" + numCalc + "posVal:" + positionValue + "posValues" + posValues);

        //return positionValue;

        return aivalue * (1.0d - bricksLeftRatio) + 0.7 * positionValue * bricksLeftRatio;
    }
}
