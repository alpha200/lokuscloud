package main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public final class Lokusserver {
    public static void main(String[] args) throws IOException {

        final Logger logger = LoggerFactory.getLogger(Lokusserver.class);

        final ExecutorService pool = Executors.newCachedThreadPool();
        final ServerSocket serverSocket = new ServerSocket(13742);

        logger.info("Listening on 13742");

        CalculationHandler calculationHandler = new CalculationHandler();
        calculationHandler.run();

        Thread serverThread = new Thread(new Server(pool, serverSocket, calculationHandler));

        serverThread.start();

        Runtime.getRuntime().addShutdownHook(
                new Thread() {
                    public void run() {
                        logger.info("Server shutdown");
                        pool.shutdown();
                        try {
                            pool.awaitTermination(2L, TimeUnit.SECONDS);
                            if (!serverSocket.isClosed()) {
                                serverSocket.close();
                            }
                        } catch (IOException | InterruptedException e) {
                        }
                    }
                }
        );
    }
}
