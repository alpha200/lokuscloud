package main;

import model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

public final class CalculationHandler{
    private final Logger logger = LoggerFactory.getLogger(CalculationHandler.class);
    private FieldNode rootNode;
    private ExecutorService pool;
    private AtomicLong nodesRunning;
    private long startTime;

    private ScoreAIValueCalculator aiValueCalculator;

    private List<TurnRequester> turnRequests;
    private ShrinkedPlayer playerToCalculateFor;

    private final AvailablePlayersDetector availablePlayersDetector;

    public CalculationHandler() {
        this.nodesRunning = new AtomicLong(0);
        this.turnRequests = new ArrayList<>();

        aiValueCalculator = new ScoreAIValueCalculator();

        pool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

        availablePlayersDetector = new AvailablePlayersDetector();
    }

    public boolean calculationIsRunning() {
        return nodesRunning.get() != 0;
    }

    public void transmitTurn(ShrinkedTurn turn) {
        for (TurnRequester turnRequester : turnRequests) {
            turnRequester.turnRequestCallback(turn);
        }
    }

    public void nodeFinishedCallback() {
        long currentRunning = nodesRunning.decrementAndGet();

        if(currentRunning == 0) {
            System.out.println("Calculation finished!");
            long difference = System.nanoTime() - startTime;

            /*for(FieldNode fn : rootNode.getChildren().values()) {
                CalculationHelpers.printField(fn.getData().getField());
            }*/

            System.out.println("Time needed: " + difference / 1000000.0f + "ms");

            // Falls als nächstes der Spieler dran ist Zug übermitteln
            if(rootNode.getBestTurn() != null) {
                // Test print
                System.out.println("Worst AI-Value: " + rootNode.getWorstAIValue());
                System.out.println(new Turn(rootNode.getBestTurn()).getBrick().getStandardbricknr());
                System.out.println(rootNode.getBestTurn().getPlayer().getColor());
                System.out.println(rootNode.getBestTurn().getCoord());
                CalculationHelpers.printBrick(rootNode.getBestTurn().getBrick().getForms()[rootNode.getBestTurn().getBrickFormNumber()]);

                transmitTurn(rootNode.getBestTurn());
            }
            else {
                System.out.println("Im out!");
            }
        }
    }

    public void addCalculationRequester(TurnRequester turnRequester) {
        turnRequests.add(turnRequester);
        if(rootNode != null && rootNode.getBestTurn() != null) {
            turnRequester.turnRequestCallback(rootNode.getBestTurn());
        }
    }

    public void removeCalculationRequester(TurnRequester turnRequester) {
        turnRequests.remove(turnRequester);
    }

    public void nodeStarted() {
        nodesRunning.incrementAndGet();
    }

    public synchronized void applyTurn(ShrinkedTurn turn) {
        System.out.println("Turn transmitted!");

        /*if(calculationIsRunning()) {
            pool.shutdown();
            try {
                pool.awaitTermination(200L, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            pool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
            nodesRunning = new AtomicLong();
        }*/

        GameState newGameState = new GameState(rootNode.getData());

        newGameState.doTurn(turn);

        while(true) {
            int nextPlayer = availablePlayersDetector.getAndSetNextPlayer();

            if(nextPlayer != -1) {
                rootNode = new FieldNode(newGameState, null, nextPlayer, turn);

                if(CalculationHelpers.calculatePossibleTurns(rootNode.getData().getField(), rootNode.getData().getPlayers()[nextPlayer]).length == 0) {
                    availablePlayersDetector.setPlayerOut(nextPlayer);
                }
                else {
                    System.out.println("Next player: " + nextPlayer);
                    break;
                }
            }
            else {
                // Kein Spieler kann mehr legen
                System.out.println("Niemand kann mehr legen!");
                return;
            }
        }

        ShrinkedPlayer nextPlayer = rootNode.getData().getPlayers()[rootNode.getCurrentPlayer()];

        if(nextPlayer.equals(playerToCalculateFor)) {
            System.out.println("Player is next -> calculating...");

            if(nextPlayer.getBricks().size() >= 19) {
                makeOpeningTurn(nextPlayer);
            }
            else {
                startCalculation(rootNode);
            }
        }
    }

    private void makeOpeningTurn(ShrinkedPlayer nextPlayer) {
        System.out.println("Doing opening turn");

        ShrinkedTurn defaultTurn = null;

        if(nextPlayer.getBricks().size() == 21) {
            ShrinkedBrick theBrick = new ShrinkedBrick(14);

            switch (nextPlayer.getColor()) {
                case BLUE:
                    defaultTurn = new ShrinkedTurn(nextPlayer, theBrick, new Point(17, 17), 2);
                    break;
                case YELLOW:
                    defaultTurn = new ShrinkedTurn(nextPlayer, theBrick, new Point(0, 17), 0);
                    break;
                case RED:
                    defaultTurn = new ShrinkedTurn(nextPlayer, theBrick, new Point(0, 0), 1);
                    break;
                case GREEN:
                    defaultTurn = new ShrinkedTurn(nextPlayer, theBrick, new Point(17, 0), 3);
                    break;
                default:
                    System.err.println("Unexpected color!");
                    break;
            }
        }
        else if(nextPlayer.getBricks().size() == 20) {
            ShrinkedBrick theBrick = new ShrinkedBrick(20);

            switch (nextPlayer.getColor()) {
                case BLUE:
                    defaultTurn = new ShrinkedTurn(nextPlayer, theBrick, new Point(14, 14), 2);
                    break;
                case YELLOW:
                    defaultTurn = new ShrinkedTurn(nextPlayer, theBrick, new Point(3, 14), 0);
                    break;
                case RED:
                    defaultTurn = new ShrinkedTurn(nextPlayer, theBrick, new Point(3, 3), 2);
                    break;
                case GREEN:
                    defaultTurn = new ShrinkedTurn(nextPlayer, theBrick, new Point(14, 3), 0);
                    break;
                default:
                    System.err.println("Unexpected color!");
                    break;
            }
        }
        else {
            ShrinkedBrick theBrick = new ShrinkedBrick(18);
            switch (nextPlayer.getColor()) {
                case BLUE:
                    defaultTurn = new ShrinkedTurn(nextPlayer, theBrick, new Point(11, 11), 1);
                    break;
                case YELLOW:
                    defaultTurn = new ShrinkedTurn(nextPlayer, theBrick, new Point(6, 11), 4);
                    break;
                case RED:
                    defaultTurn = new ShrinkedTurn(nextPlayer, theBrick, new Point(6, 6), 6);
                    break;
                case GREEN:
                    defaultTurn = new ShrinkedTurn(nextPlayer, theBrick, new Point(11, 6), 0);
                    break;
                default:
                    System.err.println("Unexpected color!");
                    break;
            }
        }

        transmitTurn(defaultTurn);
    }

    public void startCalculation(FieldNode fieldNode) {
        startTime = System.nanoTime();

        if(fieldNode.getChildren().size() != 0) {
            fieldNode.getChildren().forEach(this::startCalculation);
        }
        else {
            SubnodeCalculator snc = new SubnodeCalculator(pool, fieldNode, this);
            pool.execute(snc);
        }
    }

    public void setPlayerToCalculateFor(ShrinkedPlayer player) {
        this.playerToCalculateFor = player;

        logger.info("Player set, calculation started!");

        if(player.getColor() == Block.BLUE)
            makeOpeningTurn(player);
    }

    public void run() {
        logger.info("New calculation handler started...");

        ShrinkedPlayer player1 = new ShrinkedPlayer(Block.BLUE);
        ShrinkedPlayer player2 = new ShrinkedPlayer(Block.YELLOW);
        ShrinkedPlayer player3 = new ShrinkedPlayer(Block.RED);
        ShrinkedPlayer player4 = new ShrinkedPlayer(Block.GREEN);

        Block[][] field = new Block[20][20];

        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                field[i][j] = Block.EMPTY;
            }
        }

        GameState gameState = new GameState(field, new ShrinkedPlayer[]{player1, player2, player3, player4});

        rootNode = new FieldNode(gameState, null, 0, null);

        Runtime.getRuntime().addShutdownHook(
                new Thread() {
                    public void run() {
                        pool.shutdown();
                        try {
                            pool.awaitTermination(2L, TimeUnit.SECONDS);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
        );
    }

    public ScoreAIValueCalculator getAiValueCalculator() {
        return aiValueCalculator;
    }

    public ShrinkedPlayer getPlayerToCalculateFor() {
        return playerToCalculateFor;
    }

    public AvailablePlayersDetector getAvailablePlayersDetector() {
        return availablePlayersDetector;
    }
}
