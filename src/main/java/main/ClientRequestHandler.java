package main;

import model.ShrinkedPlayer;
import model.ShrinkedTurn;
import model.Turn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public final class ClientRequestHandler implements Runnable, TurnRequester {
    private final Logger logger = LoggerFactory.getLogger(ClientRequestHandler.class);
    private Socket client;
    ObjectInputStream objectInputStream;
    ObjectOutputStream objectOutputStream;
    CalculationHandler calculationHandler;

    public ClientRequestHandler(Socket client, CalculationHandler calculationHandler) {
        this.client = client;
        this.calculationHandler = calculationHandler;
    }

    @Override
    public void run() {
        try {
            objectInputStream = new ObjectInputStream(client.getInputStream());
            objectOutputStream = new ObjectOutputStream(client.getOutputStream());

            calculationHandler.addCalculationRequester(this);

            Object inputObject;

            while (!client.isClosed()) {
                inputObject = objectInputStream.readObject();

                if(inputObject instanceof ShrinkedTurn) {
                    ShrinkedTurn turn = (ShrinkedTurn)inputObject;

                    calculationHandler.applyTurn(turn);
                }
                else if(inputObject instanceof ShrinkedPlayer) {
                    ShrinkedPlayer player = (ShrinkedPlayer)inputObject;
                    calculationHandler.setPlayerToCalculateFor(player);
                }
                else {
                    logger.warn("Unknown Object transferred. Ignoring...");
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        finally {
            calculationHandler.removeCalculationRequester(this);
        }
    }

    @Override
    public void turnRequestCallback(ShrinkedTurn shrinkedTurn) {
        try {
            objectOutputStream.writeObject(shrinkedTurn);
        } catch (IOException e) {
            e.printStackTrace();
            calculationHandler.removeCalculationRequester(this);
        }
    }
}
