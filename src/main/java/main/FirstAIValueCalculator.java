package main;

import java.awt.Point;
import java.util.List;

import model.Block;
import model.FieldNode;
import model.GameState;
import model.ShrinkedBrick;
import model.ShrinkedTurn;

public class FirstAIValueCalculator implements AIValueCalculator {
    /**
     * Rates the current Turn. Included values are the turns of enemy players that get contered, new generated own turns,
     * field zone for the new brick and the bricks own value.
     * @return rateValue of the current turn
     */

    public double ratePossibleTurn(ShrinkedTurn turnDone, FieldNode fnAfterTurn){
        int value = turnDone.getBrick().getValue();
        int cTurns = getBestZone(turnDone, fnAfterTurn.getParent().getData().getField());
        int newTurns = getNewTurnCount(turnDone, fnAfterTurn.getParent().getData().getField());
        return value + cTurns + newTurns;
    }
    /**
     * Counts all enemy turns that get blocked by the actual own turn
     * @param turn the turn
     * @param state the state
     * @return count of enemy turns contered
     */


    private double getConteredTurns(ShrinkedTurn turn, Block[][] oldField){
        double count= 0;
    	ShrinkedBrick turnBrick = turn.getBrick();
    	int actualBrick = turn.getBrickFormNumber();
    	boolean[][] actualBrickCoords = turnBrick.getForms()[actualBrick];
    	List<Point> corner = CalculationHelpers.calculateCorners(oldField, turn.getPlayer().getColor());
    	for(Point actual : corner){
    		if(oldField[actual.x][actual.y]!=Block.EMPTY && oldField[actual.x][actual.y]!=turn.getPlayer().getColor()){
    			count++;
    		}
    	}
//    	for(int i = actualBrickCoords.length-1; i>0; i--){
//    		for(int j=0; j < actualBrickCoords[0].length;j++){
//    			if(actualBrickCoords[i][j]){
//	    			if((i-1>=0 && j-1 >=0) &&oldField[i-1][j-1] != Block.EMPTY && oldField[i-1][j-1]!= turn.getPlayer().getColor()){
//	    				count++;
//	    			}
//	    			if((i-1>=0 && j+1 < oldField[i-1].length) &&oldField[i-1][j+1] != Block.EMPTY && oldField[i-1][j+1]!= turn.getPlayer().getColor()){
//	    				count++;
//	    			}
//	    			if((i+1 < oldField.length && j-1 >=0 )&& oldField[i+1][j-1] != Block.EMPTY && oldField[i+1][j-1]!= turn.getPlayer().getColor()){
//	    				count++;
//	    			}
//
//	    			if((i+1<=oldField.length && j+1 < oldField[i-1].length) && oldField[i+1][j+1] != Block.EMPTY && oldField[i+1][j+1]!= turn.getPlayer().getColor()){
//	    				count++;
//	    			}
//    			}
//    		}
//    	}
        return count;
    }

    /**
     * Counts new possible turns
     * @param turn
     * @param field
     * @return count of new generated turns
     */
    private int getNewTurnCount(ShrinkedTurn turn, Block[][] field){
        int count = 0;
        Point[][] corners = turn.getBrick().getCornerBlocks();
        for(Point corner : corners[turn.getBrickFormNumber()]){
            if(prooveCorner(corner, field)>0){
                count +=2*prooveCorner(corner, field);
            }
        }
        return count;
    }

    private int prooveCorner(Point actualPoint, Block[][] field){
        int count = 0;
        int x = actualPoint.x;
        int y = actualPoint.y;
        boolean top, bottom, left, right;
        top = x>0;
        bottom = x<field.length-1;
        left = y>0;
        right = y<field[x].length-1;

        if(top && left){
            if(field[x-1][y-1] == Block.EMPTY){
                count ++;
            }
        }
        if(top && right){
            if(field[x-1][y+1]== Block.EMPTY){
                count++;
            }
        }
        if(bottom && left){
            if(field[x+1][y-1]== Block.EMPTY){
                count++;
            }
        }
        if(bottom && right){
            if(field[x+1][y+1]== Block.EMPTY){
                count++;
            }
        }
        return count;
    }

    private int getBestZone(ShrinkedTurn turn, Block[][] field){
        int x;
        int y;
        ShrinkedBrick turnBrick = turn.getBrick();
        int actualBrick = turn.getBrickFormNumber();
        boolean[][] actualBrickCoords = turnBrick.getForms()[actualBrick];
        Point brickStartCoord = turn.getCoord();
        x= brickStartCoord.x;
        y= brickStartCoord.y;
        for(int i=x;i< x+actualBrickCoords.length;i++){
            for(int j=y; j< y+actualBrickCoords[0].length; j++){
                if(i>= field.length/2-2 && i<=field.length/2+2 && j >= field[i].length/2 -2 && j<= field[i].length/2+2){
                    return 3;
                }
                if(i>= field.length/2-4 && i<=field.length/2+4 && j >= field[i].length/2 -4 && j<= field[i].length/2+4){
                    return 2;
                }
                if(i>= field.length/2-6 && i<=field.length/2+6 && j >= field[i].length/2 -6 && j<= field[i].length/2+6){
                    return 1;
                }
            }
        }
        return 0;
    }
}
