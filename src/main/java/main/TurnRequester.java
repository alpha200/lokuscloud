package main;

import model.ShrinkedTurn;

public interface TurnRequester {
    void turnRequestCallback(ShrinkedTurn shrinkedTurn);
}
