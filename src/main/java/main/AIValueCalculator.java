package main;

import model.FieldNode;
import model.ShrinkedPlayer;
import model.ShrinkedTurn;

public interface AIValueCalculator {
    double ratePossibleTurn(ShrinkedTurn turnDone, FieldNode fnAfterTurn);

    default double ratePossibleTurn(ShrinkedTurn turnDone, FieldNode fnAfterTurn, ShrinkedPlayer player) {
        throw new UnsupportedOperationException("Not implemented!");
    }
}
