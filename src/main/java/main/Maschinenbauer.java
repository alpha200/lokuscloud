package main;

import model.*;

import java.awt.*;

public class Maschinenbauer {
	private GameState game;
	private ShrinkedPlayer player;
	private Block color;
	private FieldNode node;
	private int turnCount;
	private AIValueCalculator aiValueCalculator;
	private int pointsEarned;
	private int ownNo;
	
	
	public Maschinenbauer(GameState newGame, ShrinkedPlayer mplayer){
		this.game = newGame;
		player = mplayer;
		turnCount= 0;
		aiValueCalculator = new FirstAIValueCalculator();
		pointsEarned = 0;
		if(mplayer.getColor()==Block.BLUE){
			ownNo = 0;
		}
		if(mplayer.getColor()==Block.YELLOW){
			ownNo = 1;
		}
		if(mplayer.getColor()==Block.RED){
			ownNo = 2;
		}
		if(mplayer.getColor()==Block.GREEN){
			ownNo = 3;
		}
	}
	
	public int getPointsEarned() {
		return pointsEarned;
	}

	public void setPointsEarned(int pointsEarned) {
		this.pointsEarned = pointsEarned;
	}

	public AIValueCalculator getAiValueCalculator() {
		return aiValueCalculator;
	}

	public void setAiValueCalculator(AIValueCalculator aiValueCalculator) {
		this.aiValueCalculator = aiValueCalculator;
	}

	public ShrinkedTurn giveBestTurn(GameState actualGame){
		game = actualGame;
		ShrinkedTurn bestTurn;
		if(turnCount>=3){
			FieldNode fn = new FieldNode(new GameState(game), null, ownNo);
			ShrinkedTurn[] allTurns = CalculationHelpers.calculatePossibleTurns(game.getField(), player);
			for(int i=0;i< allTurns.length;i++){
				
				
				FieldNode next = new FieldNode(new GameState(fn.getData()), fn, ownNo);
				allTurns[i].setAiValue(aiValueCalculator.ratePossibleTurn(allTurns[i], next));
			}
			if(allTurns.length>0){
				bestTurn = giveMaxTurn(allTurns);
				game.doTurn(bestTurn);
				pointsEarned += bestTurn.getBrick().getValue();
				return bestTurn;
			}
		}
		else{
			if(player.getColor()==Block.BLUE){
				bestTurn = moveFirstThreeWithBlue();
				game.doTurn(bestTurn);
				turnCount++;
				pointsEarned += bestTurn.getBrick().getValue();
				return bestTurn;
			}
			if(player.getColor()==Block.GREEN){
				bestTurn = moveFirstThreeWithGreen();
				game.doTurn(bestTurn);
				turnCount++;
				pointsEarned += bestTurn.getBrick().getValue();
				return bestTurn;
			}
			if(player.getColor()==Block.RED){
				bestTurn = moveFirstThreeWithRed();
				game.doTurn(bestTurn);
				turnCount++;
				pointsEarned += bestTurn.getBrick().getValue();
				return bestTurn;
			}
			if(player.getColor()==Block.YELLOW){
				bestTurn = moveFirstThreeWithYellow();
				game.doTurn(bestTurn);
				turnCount++;
				pointsEarned += bestTurn.getBrick().getValue();
				return bestTurn;
			}
		}

		return null;
	}
	//Spieler unten rechts
	private ShrinkedTurn moveFirstThreeWithBlue(){
		ShrinkedBrick brick;
		if(turnCount ==0){
			brick = new ShrinkedBrick(Brick.getStandardBrick(14));
			return new ShrinkedTurn(player, brick, new Point(17,17), 1);
		}
		if(turnCount ==1){
			brick = new ShrinkedBrick(Brick.getStandardBrick(19));
			return new ShrinkedTurn(player, brick, new Point(14,14), 1);
		}
		if(turnCount ==2){
			brick = new ShrinkedBrick(Brick.getStandardBrick(20));
			return new ShrinkedTurn(player, brick, new Point(11,11), 1);
		}
		return null;
	}
	//Spieler oben links
	private ShrinkedTurn moveFirstThreeWithRed(){
		ShrinkedBrick brick;
		if(turnCount ==0){
			brick = new ShrinkedBrick(Brick.getStandardBrick(19));
			return new ShrinkedTurn(player, brick, new Point(0,0), 1);
		}
		if(turnCount ==1){
			brick = new ShrinkedBrick(Brick.getStandardBrick(14));
			return new ShrinkedTurn(player, brick, new Point(3,3), 1);
		}
		if(turnCount ==2){
			brick = new ShrinkedBrick(Brick.getStandardBrick(20));
			return new ShrinkedTurn(player, brick, new Point(6,6), 1);
		}
		return null;
	}
	//Spieler unten links
	private ShrinkedTurn moveFirstThreeWithYellow(){
		ShrinkedBrick brick;
		if(turnCount ==0){
			brick = new ShrinkedBrick(Brick.getStandardBrick(19));
			return new ShrinkedTurn(player, brick, new Point(0,17), 0);
		}
		if(turnCount ==1){
			brick = new ShrinkedBrick(Brick.getStandardBrick(14));
			return new ShrinkedTurn(player, brick, new Point(3,14), 0);
		}
		if(turnCount ==2){
			brick = new ShrinkedBrick(Brick.getStandardBrick(20));
			return new ShrinkedTurn(player, brick, new Point(6,11), 0);
		}
		return null;
	}
	//Spieler oben rechts
	private ShrinkedTurn moveFirstThreeWithGreen(){
		ShrinkedBrick brick;
		if(turnCount ==0){
			brick = new ShrinkedBrick(Brick.getStandardBrick(19));
			return new ShrinkedTurn(player, brick, new Point(17,0), 0);
		}
		if(turnCount ==1){
			brick = new ShrinkedBrick(Brick.getStandardBrick(14));
			return new ShrinkedTurn(player, brick, new Point(14,3), 0);
		}
		if(turnCount ==2){
			brick = new ShrinkedBrick(Brick.getStandardBrick(20));
			return new ShrinkedTurn(player, brick, new Point(11,6), 0);
		}
		return null;
	}
	
	private ShrinkedTurn giveMaxTurn(ShrinkedTurn[] turn){
		double max = turn[0].getAiValue();
		ShrinkedTurn maxTurn = turn[0];
		for(int i=1;i<turn.length;i++){
			if(turn[i].getAiValue()>max){
				max=turn[i].getAiValue();
				maxTurn = turn[i];
			}
		}
		return maxTurn;
	}
}
