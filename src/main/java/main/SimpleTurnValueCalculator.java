package main;

import model.FieldNode;
import model.ShrinkedTurn;

public final class SimpleTurnValueCalculator implements AIValueCalculator {
    @Override
    public double ratePossibleTurn(ShrinkedTurn turnDone, FieldNode fnAfterTurn) {
        return turnDone.getBrick().getValue();
    }
}
