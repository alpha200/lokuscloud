package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;

/**
 * Treenode with current gamestate and possible generated substates if calculated
 * the substates can be reached by doing the turn saved as key in the children-map
 */
public class FieldNode {
    private final GameState data;
    private final List<FieldNode> children;
    private FieldNode parent;
    private final int currentPlayer;
    private double worstAIValue;
    private final Map<ShrinkedTurn, Double> bestTurns;
    private ShrinkedTurn turnDone;
    private final Semaphore setMax = new Semaphore(1);
    private double scoreRating;

    public GameState getData() {
        return data;
    }

    public List<FieldNode> getChildren() {
        return children;
    }

    public FieldNode(GameState data, FieldNode parent, int currentPlayer) {
        this.data = data;
        this.currentPlayer = currentPlayer;
        this.children = new ArrayList<>();
        this.parent = parent;
        this.worstAIValue = Double.POSITIVE_INFINITY;
        this.bestTurns = new HashMap<>();
        this.turnDone = null;
    }

    public FieldNode(GameState data, FieldNode parent, int currentPlayer, ShrinkedTurn turnDone) {
        this.data = data;
        this.currentPlayer = currentPlayer;
        this.children = new ArrayList<>();
        this.parent = parent;
        this.worstAIValue = Double.POSITIVE_INFINITY;
        this.bestTurns = new HashMap<>();
        this.turnDone = turnDone;
    }

    public FieldNode getParent() {
        return parent;
    }

    public void setParent(FieldNode parent) {
        this.parent = parent;
    }

    public int getHeight() {
        if(parent != null) {
            return parent.getHeight() + 1;
        }
        else {
            return 0;
        }
    }
    
    public void setMaxValue(double calculatedValue, ShrinkedTurn turnOfCallingNode) {
        try {
            setMax.acquire();

            bestTurns.put(turnOfCallingNode, calculatedValue);

            if(worstAIValue >= calculatedValue) {
                worstAIValue = calculatedValue;

                if(parent != null) {
                    parent.setMaxValue(worstAIValue, turnDone);
                }
            }

            setMax.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.err.println("Acquire failed!");
        }
    }

    public int getCurrentPlayer() {
        return currentPlayer;
    }

    public FieldNode getRootNode() {
        if(this.getParent() == null) {
            return null;
        }
        else {
            FieldNode root = this;
            while(root.getParent() != null) {
                root = root.getParent();
            }

            return root;
        }
    }

    public ShrinkedTurn getBestTurn() {
        ShrinkedTurn bestTurn = null;
        double bestValue = Double.NEGATIVE_INFINITY;

        for(ShrinkedTurn st : bestTurns.keySet()) {
            if(bestTurns.get(st) > bestValue) {
                bestTurn = st;
                bestValue = bestTurns.get(st);
            }
        }

        return bestTurn;
    }

    public ShrinkedTurn getTurnDone() {
        return turnDone;
    }

    public double getWorstAIValue() {
        return worstAIValue;
    }

    public double getScoreRating() {
        return scoreRating;
    }

    public void setScoreRating(double scoreRating) {
        this.scoreRating = scoreRating;
    }
}
