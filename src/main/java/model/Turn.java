package model;

import java.awt.*;
import java.io.Serializable;


public class Turn implements Serializable { // NOPMD by sopr043 on 07.09.15 10:25. name Turn is informative enough
	private static final long serialVersionUID = 1L;

	private Player player;
	private Brick brick;
	private Point coord;

    public Turn(Player player, Brick brick, Point coord) {
        this.player = player;
        this.brick = brick;
        this.coord = coord;
    }

    public Player getPlayer() {
        return player;
    }

    public Brick getBrick() {
        return brick;
    }

    public Point getCoord() {
        return coord;
    }

    public Turn(ShrinkedTurn shrinkedTurn) {
        this.coord = new Point(shrinkedTurn.getCoord().x + 2, shrinkedTurn.getCoord().y + 2);
        this.player = new Player(shrinkedTurn.getPlayer());

        boolean[][] transformedForm = shrinkedTurn.getBrick().getForms()[shrinkedTurn.getBrickFormNumber()];

        boolean[][] brickForm = new boolean[5][5];

        for (int i = 0; i < transformedForm.length; i++) {
            System.arraycopy(transformedForm[i], 0, brickForm[i], 0, transformedForm[i].length);
        }

        this.brick = new Brick(brickForm, shrinkedTurn.getBrick().getStandardBrickNr());
    }
}
