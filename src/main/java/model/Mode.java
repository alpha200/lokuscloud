package model;

public enum Mode {

	STANDARD, BLOKUSDUO, SOLITAIR, SOLITAIRBIG, CUSTOM
}
