package model;

public final class EmptyStringException extends RuntimeException {
    public EmptyStringException(String s) {
        super(s);
    }
}
