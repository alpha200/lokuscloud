package model;

public class Score implements Comparable<Score> {

	private int points;
	private String name;
	private String date;

	public Score(int points, String name, String date) {
		this.points = points;
		this.name = name;
		this.date = date;
	}

	public int getPoints() {
		return points;
	}

	public String getName() {
		return name;
	}

	public String getDate() {
		return date;
	}

	@Override
	public int compareTo(Score comp) {
		if(this.points > comp.getPoints())
		{
			return 1;
		}
		else if(this.points < comp.getPoints())
		{
			return -1;
		}
		else
		{
			return 0;
		}
	}
}
