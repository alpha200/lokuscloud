import model.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class BrickTest {
    @Test
    public void testBrickCloneList() {
        ShrinkedBrick sb = new ShrinkedBrick(Brick.getStandardBrick(20));

        ArrayList<ShrinkedBrick> sbl = new ArrayList<>();

        sbl.add(sb);

        ArrayList<ShrinkedBrick> nsbl = new ArrayList<>();

        nsbl.add(new ShrinkedBrick(sbl.get(0)));

        sbl.remove(sb);

        Assert.assertEquals("Wroooooooooooooooooooooooooong!!!!", 0, sbl.size());
        Assert.assertEquals("WTF!", 1, nsbl.size());
    }

    @Test
    public void testPlayerClone() {
        ShrinkedPlayer p1 = new ShrinkedPlayer(Block.BLUE);

        Block[][] field = new Block[20][20];

        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                field[i][j] = Block.EMPTY;
            }
        }

        GameState gs = new GameState(field, new ShrinkedPlayer[] {p1});

        GameState gs2 = new GameState(gs);

        gs2.getPlayers()[0].getBricks().remove(0);

        Assert.assertEquals("Stein wurde nicht aus gs2 entfernt!", 20, gs2.getPlayers()[0].getBricks().size());
        Assert.assertEquals("Liste aus gs ist mit verändert worden!", 21, gs.getPlayers()[0].getBricks().size());
    }
}
