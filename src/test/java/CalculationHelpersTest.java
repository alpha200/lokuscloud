
import main.CalculationHelpers;
import model.*;
import org.junit.Test;

import java.awt.*;
import java.util.List;


public class CalculationHelpersTest {


	@Test
	public void testCalculateCorners(){
		Block[][] testField = new Block[20][20];
		for(int i=0; i<20;i++){
			for(int j=0;j<20;j++){
				testField[i][j]= Block.EMPTY;
			}
		}
		ShrinkedBrick sbrick = new ShrinkedBrick(Brick.getStandardBrick(19));
		ShrinkedBrick sbrick2 = new ShrinkedBrick(Brick.getStandardBrick(14));
		ShrinkedBrick sbrick3 = new ShrinkedBrick(Brick.getStandardBrick(20));
		
		ShrinkedPlayer player = new ShrinkedPlayer(Block.BLUE);
		ShrinkedPlayer player2 = new ShrinkedPlayer(Block.YELLOW);
		ShrinkedPlayer player3 = new ShrinkedPlayer(Block.RED);
		ShrinkedPlayer player4 = new ShrinkedPlayer(Block.GREEN);
		ShrinkedPlayer[] players = {player,player2,player3,player4};
		ShrinkedTurn testTurn = new ShrinkedTurn(player, sbrick, new Point(17,17), 1);
		ShrinkedTurn testTurn2 = new ShrinkedTurn(player, sbrick2, new Point (14,14), 1);
		ShrinkedTurn testTurn3 = new ShrinkedTurn(player, sbrick3, new Point(11,11), 1);

		
		GameState state = new GameState(testField, players);
		/*testField[19][19] = Block.BLUE;
		testField[19][18] = Block.BLUE;
		testField[18][18] = Block.BLUE;
		testField[17][18] = Block.BLUE;
		testField[17][17] = Block.BLUE;*/
		state.doTurn(testTurn);
		CalculationHelpers.printField(state.getField());
		ShrinkedTurn[] newTurns = CalculationHelpers.calculatePossibleTurns(state.getField(), state.getPlayers()[0]);
		System.out.println("Es wurden "+ newTurns.length + "neue Züge gefunden");
		state.doTurn(testTurn2);
		CalculationHelpers.printField(state.getField());
		newTurns = CalculationHelpers.calculatePossibleTurns(state.getField(), state.getPlayers()[0]);
		System.out.println("Es wurden "+ newTurns.length + "neue Züge gefunden");
		
		state.doTurn(testTurn3);
		CalculationHelpers.printField(state.getField());
		newTurns = CalculationHelpers.calculatePossibleTurns(state.getField(), state.getPlayers()[0]);
		System.out.println("Es wurden "+ newTurns.length + "neue Züge gefunden");
		List<Point> cornerList = CalculationHelpers.calculateCorners(state.getField(), Block.BLUE);
		int cornerCount = cornerList.size();
		System.out.println("Es wurden "+ cornerCount + " anlegestellen gefunden");
		for(Point xs : cornerList){
			System.out.println("Punkt: ("+ xs.x+ ", "+ xs.y + ")");
		}
		
	}

}
