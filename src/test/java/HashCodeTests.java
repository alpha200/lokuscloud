import model.*;
import org.junit.Assert;
import org.junit.Test;

import java.awt.*;

public class HashCodeTests {
    @Test
    public void testShrinkedBrickHashcodes() {
        ShrinkedBrick sb1 = new ShrinkedBrick(Brick.getStandardBrick(11));

        boolean[][] b1 = new boolean[5][5];

        b1[3][4] = b1[4][4] = b1[4][3] = b1[4][2] = b1[4][1] = true;

        Brick b2 = new Brick(b1);

        ShrinkedBrick sb2 = new ShrinkedBrick(b2);

        Assert.assertEquals("Hashcodes müssen gleich sein!", sb2.hashCode(), sb1.hashCode());
    }

    @Test
    public void testShrinkedTurnHashcodes() {
        ShrinkedBrick sb1 = new ShrinkedBrick(Brick.getStandardBrick(11));

        boolean[][] b1 = new boolean[5][5];

        b1[3][4] = b1[4][4] = b1[4][3] = b1[4][2] = b1[4][1] = true;

        Brick b2 = new Brick(b1);

        ShrinkedBrick sb2 = new ShrinkedBrick(b2);

        ShrinkedPlayer sp = new ShrinkedPlayer(Block.BLUE);

        ShrinkedTurn st1 = new ShrinkedTurn(sp, sb1, new Point(0,0), 0);
        ShrinkedTurn st2 = new ShrinkedTurn(sp, sb2, new Point(0,0), 0);

        Assert.assertEquals("Hashcodes müssen gleich sein!", st1.hashCode(), st2.hashCode());
    }
}