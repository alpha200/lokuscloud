import model.*;
import org.junit.Assert;
import org.junit.Test;

import java.awt.*;

public class ShrinkedTurnTests {
    @Test
    public void testTurnInShrinkedTurnConverter() {

        boolean[][] brickCoord = new boolean[5][5];

        brickCoord[4][1] = brickCoord[4][2] = brickCoord[4][3] = brickCoord[4][4] = brickCoord[3][4] = true;

        Turn turn = new Turn(new Player(false, Block.BLUE, "blubb", PlayerType.PLAYER), new Brick(brickCoord, 11), new Point(10, 10));

        ShrinkedTurn st = new ShrinkedTurn(turn);

        Assert.assertEquals("New x is not as expected", 11, st.getCoord().x);
        Assert.assertEquals("New y is not as expected", 9, st.getCoord().y);
    }
}
