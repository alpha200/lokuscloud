import java.awt.Point;

import main.CalculationHelpers;
import main.Maschinenbauer;
import model.Block;
import model.Brick;
import model.FieldNode;
import model.GameState;
import model.ShrinkedBrick;
import model.ShrinkedPlayer;
import model.ShrinkedTurn;

import org.junit.Test;


public class MaschinenbauerTest {

	private ShrinkedPlayer player1 = new ShrinkedPlayer(Block.BLUE);
	private ShrinkedPlayer player2 = new ShrinkedPlayer(Block.YELLOW);
	private ShrinkedPlayer player3 = new ShrinkedPlayer(Block.RED);
	private ShrinkedPlayer player4 = new ShrinkedPlayer(Block.GREEN);
	private ShrinkedPlayer[] players ={player1, player2, player3, player4};
	private Block[][] field = new Block[20][20];
	
	@Test
	public void testMaschinenbau(){
		int playerno=0;
		for(int i=0; i<20;i++){
			for(int j=0;j<20;j++){
				field[i][j]= Block.EMPTY;
			}
		}
		GameState game = new GameState(field, players);
		Maschinenbauer testplayer = new Maschinenbauer(game, player1);
		Maschinenbauer testplayer2 = new Maschinenbauer(game, player2);
		Maschinenbauer testplayer3 = new Maschinenbauer(game, player3);
		Maschinenbauer testplayer4 = new Maschinenbauer(game, player4);
		ShrinkedTurn turn;

		turn = testplayer.giveBestTurn(game);
		game.doTurn(turn);
		CalculationHelpers.printField(game.getField());
		playerno++;
//		turn = testplayer.giveBestTurn(game);
//		game.doTurn(turn);
//		CalculationHelpers.printField(game.getField());
		//Ab hier berechnete Züge
		
//		turn = testplayer.giveBestTurn(game);
//		game.doTurn(turn);
//		CalculationHelpers.printField(game.getField());
//		turn = testplayer.giveBestTurn(game);
//		game.doTurn(turn);
//		CalculationHelpers.printField(game.getField());
		
		boolean possible = true;
		while(possible){
			if(playerno%4==0){
				turn = testplayer.giveBestTurn(game);
				if(turn !=null){
					game.doTurn(turn);
					CalculationHelpers.printField(game.getField());
					playerno++;
				}
				else{
					possible = false;
				}
			}
			
			if(playerno%4==1){
				turn = testplayer2.giveBestTurn(game);
				if(turn !=null){
					game.doTurn(turn);
					CalculationHelpers.printField(game.getField());
					playerno++;
				}
				else{
					possible = false;
				}
			}
			if(playerno%4==2){
				turn = testplayer3.giveBestTurn(game);
				if(turn !=null){
					game.doTurn(turn);
					CalculationHelpers.printField(game.getField());
					playerno++;
				}
				else{
					possible = false;
				}
			}
			if(playerno%4==3){
				turn = testplayer4.giveBestTurn(game);
				if(turn !=null){
					game.doTurn(turn);
					CalculationHelpers.printField(game.getField());
					playerno++;
				}
				else{
					possible = false;
				}
			}
		}
		System.out.println("Spieler Blau: "+ testplayer.getPointsEarned());
		System.out.println("Spieler Gelb: "+ testplayer2.getPointsEarned());
		System.out.println("Spieler Rot: "+ testplayer3.getPointsEarned());
		System.out.println("Spieler Grün: "+ testplayer4.getPointsEarned());
	}
	
	@Test
	public void testConteredTurns(){
		int playerno=0;
		for(int i=0; i<20;i++){
			for(int j=0;j<20;j++){
				field[i][j]= Block.EMPTY;
			}
		}
		GameState game = new GameState(field, players);
		Maschinenbauer testplayer = new Maschinenbauer(game, player1);
		Maschinenbauer testplayer2 = new Maschinenbauer(game, player2);
		Maschinenbauer testplayer3 = new Maschinenbauer(game, player3);
		Maschinenbauer testplayer4 = new Maschinenbauer(game, player4);
		ShrinkedTurn turn;
		
		turn = testplayer.giveBestTurn(game);
		game.doTurn(turn);
		
		//CalculationHelpers.printField(game.getField());
		playerno++;
		int i=0;
		boolean possible = true;
		while(possible && i<11){
			if(playerno%4==0){
				turn = testplayer.giveBestTurn(game);
				if(turn !=null){
					game.doTurn(turn);
					//CalculationHelpers.printField(game.getField());
					playerno++;
					i++;
				}
				else{
					possible = false;
				}
			}
			
			if(playerno%4==1){
				turn = testplayer2.giveBestTurn(game);
				if(turn !=null){
					game.doTurn(turn);
					//CalculationHelpers.printField(game.getField());
					playerno++;
					i++;
				}
				else{
					possible = false;
				}
			}
			if(playerno%4==2){
				turn = testplayer3.giveBestTurn(game);
				if(turn !=null){
					game.doTurn(turn);
					//CalculationHelpers.printField(game.getField());
					playerno++;
					i++;
				}
				else{
					possible = false;
				}
			}
			if(playerno%4==3){
				turn = testplayer4.giveBestTurn(game);
				if(turn !=null){
					game.doTurn(turn);
					//CalculationHelpers.printField(game.getField());
					playerno++;
					i++;
				}
				else{
					possible = false;
				}
			}
		}
		FieldNode node = new FieldNode(new GameState (game), null, 0);
		ShrinkedBrick testBrick = new ShrinkedBrick(Brick.getStandardBrick(10));
		ShrinkedTurn testTurn = new ShrinkedTurn(player1 ,testBrick,new Point(6,10),0);
		game.doTurn(testTurn);
		CalculationHelpers.printField(node.getData().getField());
		FieldNode next = new FieldNode(game, node, 0);
		testplayer.getAiValueCalculator().ratePossibleTurn(testTurn, next);
		CalculationHelpers.printField(game.getField());
		
	}

}
